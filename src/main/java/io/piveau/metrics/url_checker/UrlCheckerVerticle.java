package io.piveau.metrics.url_checker;

import io.piveau.dqv.PiveauMetrics;
import io.piveau.json.ConfigHelper;
import io.piveau.pipe.PipeContext;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.utils.JenaUtils;
import io.piveau.dcatap.FormatVisitor;
import io.piveau.vocabularies.vocabulary.DQV;
import io.piveau.vocabularies.vocabulary.PV;
import io.vertx.core.*;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonObject;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.*;
import org.ehcache.UserManagedCache;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.UserManagedCacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class UrlCheckerVerticle extends AbstractVerticle {

    static final String ADDRESS = "io.piveau.pipe.check.url.queue";

    private static final Logger log = LoggerFactory.getLogger(UrlCheckerVerticle.class);

    private final UrlValidator validator =
            new UrlValidator(UrlValidator.ALLOW_ALL_SCHEMES + UrlValidator.ALLOW_2_SLASHES + UrlValidator.ALLOW_LOCAL_URLS);

    private HttpClient httpClient;

    private int timeout;
    private String userAgent;

    private UserManagedCache<String, Integer> cache;

    @Override
    public void start(Promise<Void> startPromise) {

        cache = UserManagedCacheBuilder.newUserManagedCacheBuilder(String.class, Integer.class)
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(5)))
                .build(true);

        JsonObject connectionConfig = ConfigHelper.forConfig(config()).forceJsonObject(ApplicationConfig.ENV_PIVEAU_URLCHECKER_CLIENT_CONNECTION_CONFIG);
        boolean keepAlive = connectionConfig.getBoolean("keepAlive", false);

        timeout = config().getInteger(ApplicationConfig.ENV_TIMEOUT_IN_MILLIS, ApplicationConfig.DEFAULT_TIMEOUT_IN_MILLIS);
        timeout = connectionConfig.getInteger("timeout", timeout);

        int poolSize = config().getInteger(
                ApplicationConfig.ENV_PIVEAU_URLCHECKER_CLIENT_POOL_SIZE,
                ApplicationConfig.DEFAULT_PIVEAU_URLCHECKER_CLIENT_POOL_SIZE);
        poolSize = connectionConfig.getInteger("poolSize", poolSize);

        userAgent = config().getString(ApplicationConfig.ENV_HTTP_USER_AGENT, ApplicationConfig.DEFAULT_HTTP_USER_AGENT);
        userAgent = connectionConfig.getString("userAgent", userAgent);

        HttpClientOptions httpClientOptions = new HttpClientOptions()
                .setConnectTimeout(timeout)
                .setMaxPoolSize(poolSize)
                .setKeepAlive(keepAlive);

        httpClient = vertx.createHttpClient(httpClientOptions);

        vertx.eventBus().consumer(ADDRESS, this::handleIncomingPipe);

        startPromise.complete();
    }

    private void handleIncomingPipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        if (RDFMimeTypes.TRIG.equalsIgnoreCase(pipeContext.getMimeType())) {
            try {
                Dataset dataset = JenaUtils.readDataset(pipeContext.getStringData().getBytes(), null);
                pipeContext.getPipeManager().freeData();

                incomingDataset(dataset, pipeContext);
            } catch (Exception e) {
                pipeContext.log().error("General error!", e);
                pipeContext.pass();
            }
        } else {
            pipeContext.pass();
        }
    }

    private void incomingDataset(Dataset dataset, PipeContext pipeContext) {
        Model defaultModel = dataset.getDefaultModel();
        List<Model> metricsModels = PiveauMetrics.listMetricsModels(dataset);
        Model metricsModel;
        if (metricsModels.isEmpty()) {
            String uri = "urn:" + JenaUtils.normalize(pipeContext.getPipe().getHeader().getName()) + ":" + pipeContext.getPipe().getHeader().getName();
            metricsModel = PiveauMetrics.createMetricsGraph(dataset, uri);
            defaultModel.listSubjectsWithProperty(RDF.type, DCAT.Dataset).forEachRemaining(res -> metricsModel.add(res, DQV.hasQualityMetadata, metricsModel.getResource(uri)));
        } else {
            metricsModel = metricsModels.get(0);
        }

        List<JsonObject> requests = extractUrls(defaultModel);

        List<Future<Void>> checks = new ArrayList<>();
        requests.forEach(req -> {
            String url = req.containsKey("accessUrl") ? req.getString("accessUrl") : req.getString("downloadUrl");
            String method = req.getString("method");
            Promise<Void> promise = Promise.promise();
            checks.add(promise.future());
            checkUrl(url, method)
                    .onSuccess(check -> {
                        pipeContext.log().debug("URL {} check: {} - {}", url, check.getInteger("status"), check.getString("note"));
                        req.put("status", check.getInteger("status"));

                        // if replace
                        Resource dist = metricsModel.getResource(req.getString("distributionUri"));
                        Resource urlMetric = req.containsKey("accessUrl") ? PV.accessUrlStatusCode : PV.downloadUrlStatusCode;
                        Resource url1 = metricsModel.createResource(req.containsKey("accessUrl") ? req.getString("accessUrl") : req.getString("downloadUrl"));

                        PiveauMetrics.removeMeasurement(metricsModel, dist, urlMetric, url1);
                        PiveauMetrics.addMeasurement(metricsModel, dist, url1, urlMetric, check.getInteger("status"));
                        promise.complete();
                    })
                    .onFailure(cause -> {
                        pipeContext.log().error("URL check for {} failed.", pipeContext.getDataInfo().getString("uriRef", pipeContext.getDataInfo().getString("identifier")), cause);
                        promise.complete();
                    });
        });

        CompositeFuture.join(new ArrayList<>(checks))
                .onSuccess(c -> {
                    pipeContext.log().info("Checked " + checks.size() + " URLs: " + pipeContext.getDataInfo().encode());
                    pipeContext.setResult(JenaUtils.write(dataset, RDFFormat.TRIG), RDFMimeTypes.TRIG, pipeContext.getDataInfo()).forward();
                    dataset.close();
                })
                .onFailure(cause -> {
                    pipeContext.log().error("URL checks for " + pipeContext.getDataInfo().getString("uriRef", pipeContext.getDataInfo().getString("identifier")) + " failed.", cause);
                    pipeContext.pass();
                    dataset.close();
                });
    }

    public Future<JsonObject> checkUrl(String url, String method) {
        return Future.future(promise -> {
            JsonObject result = new JsonObject();
            if (!validator.isValid(url)) {
                log.debug("Encountered invalid URL: {}", url);
                result.put("status", config().getInteger(ApplicationConfig.ENV_INVALID_URL_STATUS, ApplicationConfig.DEFAULT_INVALID_URL_STATUS));
                result.put("note", "URL is invalid");
                promise.complete(result);
            } else {
                try {
                    String parsed = new URIBuilder(url).removeQuery().build().toString();
                    if (cache.containsKey(parsed)) {
                        result.put("status", cache.get(parsed));
                        result.put("note", "Cached value");
                        promise.complete(result);
                    } else {
                        HttpMethod httpMethod;
                        String finalURL;
                        if (method.equals("HEAD")) {
                            httpMethod = HttpMethod.HEAD;
                            finalURL = url;
                        } else {
                            httpMethod = HttpMethod.GET;
                            finalURL = getGeoUrl(url, method);
                        }

                        performUrlCheck(finalURL, httpMethod, false, parsed, result)
                                .onSuccess(ar -> promise.complete(result))
                                .onFailure(cause -> {
                                    log.debug("Connection failure: {}", cause.getMessage());
                                    result.put("status", config().getInteger(ApplicationConfig.ENV_CONNECTION_EXCEPTION_STATUS, ApplicationConfig.DEFAULT_CONNECTION_EXCEPTION_STATUS));
                                    result.put("note", cause.getMessage());
                                    cache.putIfAbsent(parsed, result.getInteger("status"));
                                    promise.complete(result);
                                });
                    }
                } catch (IllegalArgumentException | VertxException e) {
                    log.error("Checking url {}", url, e);
                    result.put("status", config().getInteger(ApplicationConfig.ENV_INVALID_URL_STATUS, ApplicationConfig.DEFAULT_INVALID_URL_STATUS));
                    result.put("note", e.getMessage());
                    promise.complete(result);
                } catch (Exception e) {
                    log.error("Checking url {}", url, e);
                    promise.fail(e);
                }
            }
        });
    }

    public Future<Void> performUrlCheck(String url,
                                        HttpMethod method,
                                        boolean closeConnectionAfterHeader,
                                        String parsed,
                                        JsonObject result) {
        return Future.future(performUrlCheckPromise -> {
            log.debug("Check with {}: {}", method.toString(), url);
            httpClient.request(new RequestOptions()
                            .setMethod(method)
                            .setFollowRedirects(true)
                            .setAbsoluteURI(url))
                    .onSuccess(httpClientRequest -> {
                        httpClientRequest.putHeader(HttpHeaders.USER_AGENT, userAgent);
                        httpClientRequest.setTimeout(timeout).send()
                                .onSuccess(httpClientResponse -> {
                                    // if HEAD not supported (405 Method not allowed), then GET with closeConnectionAfterHeader = true
                                    if (method.equals(HttpMethod.HEAD) && httpClientResponse.statusCode() == 405) {
                                        performUrlCheck(url, HttpMethod.GET, true, parsed, result)
                                                .onSuccess(ar -> performUrlCheckPromise.complete())
                                                .onFailure(performUrlCheckPromise::fail);
                                    } else {
                                        if (cache.containsKey(parsed)) {
                                            result.put("status", cache.get(parsed));
                                            result.put("note", "Cached value, after response");
                                        } else {
                                            result.put("status", httpClientResponse.statusCode());
                                            result.put("note", "Response value");
                                        }
                                        cache.putIfAbsent(parsed, result.getInteger("status"));

                                        if (closeConnectionAfterHeader) {
                                            // Suppress handled Exception, that is raised and logged by httpClientResponse after the connection is closed,
                                            // but the body is not finally received.
                                            httpClientResponse.exceptionHandler(exception -> { });
                                            httpClientRequest.connection().close();
                                            performUrlCheckPromise.complete();
                                        } else {
                                            // Clean solution: If in case of GET Method connection is not closed manually, wait until body is downloaded (same behaviour like using WebClient)
                                            // "Dirty" solution: Ignore the body and complete Future immediately. This may lead to Log entries "Connection closed"
                                            httpClientResponse.end().onComplete(ar -> performUrlCheckPromise.complete());
                                        }
                                    }
                                })
                                .onFailure(performUrlCheckPromise::fail);
                    })
                    .onFailure(performUrlCheckPromise::fail);
        });
    }

    public List<JsonObject> extractUrls(Model model) {
        List<JsonObject> extracted = new ArrayList<>();
        model.listSubjectsWithProperty(RDF.type, DCAT.Dataset).forEachRemaining(dataset ->
                dataset.listProperties(DCAT.distribution).forEachRemaining(statement -> {
                    Resource distribution = statement.getResource();
                    JsonObject jsonObject = new JsonObject()
                            .put("datasetUri", dataset.getURI())
                            .put("distributionUri", distribution.getURI());

                    String method = "HEAD";

                    if (distribution.hasProperty(DCTerms.format)) {
                        JsonObject format = (JsonObject) distribution.listProperties(DCTerms.format).next().getObject().visitWith(FormatVisitor.INSTANCE);
                        if (!format.isEmpty()) {
                            switch (format.getString("id", "").toLowerCase()) {
                                case "wms":
                                case "wms_srvc": {
                                    method = "WMS";
                                    break;
                                }
                                case "wfs":
                                case "wfs_srvc": {
                                    method = "WFS";
                                    break;
                                }
                                case "wmts": {
                                    method = "WMTS";
                                    break;
                                }
                                case "wcs": {
                                    method = "WCS";
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }
                            log.debug("Extraction method for {}: {}", format.getString("id"), method);
                        }
                    }
                    jsonObject.put("method", method);

                    distribution.listProperties(DCAT.accessURL).forEachRemaining(st -> {
                        if (st.getObject().isURIResource()) {
                            extracted.add(jsonObject.copy().put("accessUrl", st.getResource().getURI()));
                        }
                    });

                    distribution.listProperties(DCAT.downloadURL).forEachRemaining(st -> {
                        if (st.getObject().isURIResource()) {
                            extracted.add(jsonObject.copy().put("downloadUrl", st.getResource().getURI()));
                        }
                    });
                })
        );
        return extracted;
    }

    private String getGeoUrl(String url, String method) {
        try {
            URIBuilder builder = new URIBuilder(url);
            if (!builder.getQueryParams().contains(new BasicNameValuePair("request", "GetCapabilities"))
                    && !builder.getQueryParams().contains(new BasicNameValuePair("REQUEST", "GetCapabilities"))) {
                builder.clearParameters().setParameter("request", "GetCapabilities").setParameter("service", method);
            }
            return builder.build().toString();
        } catch (URISyntaxException e) {
            log.warn("Parsing geo URL: {}", url, e);
            return url;
        }
    }

    /**
     * Helper method for testing
     */
    public void InitTest(Vertx vertx) {
        cache = UserManagedCacheBuilder.newUserManagedCacheBuilder(String.class, Integer.class)
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(5)))
                .build(false);
        cache.init();
        timeout = 10000;

        HttpClientOptions httpClientOptions = new HttpClientOptions()
                .setConnectTimeout(10000)
                .setMaxPoolSize(100)
                .setKeepAlive(true);
        httpClient = vertx.createHttpClient(httpClientOptions);
        userAgent = "Mozilla/5.0 (European Data Portal) Gecko/20100101 Firefox/40.1";
    }
}
