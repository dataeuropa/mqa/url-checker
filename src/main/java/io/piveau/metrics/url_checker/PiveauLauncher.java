package io.piveau.metrics.url_checker;

import io.vertx.core.Launcher;
import io.vertx.core.VertxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class PiveauLauncher extends Launcher {
    private Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void beforeStartingVertx(VertxOptions options) {
        log.info("Configure vertx the piveau way...");
        options.getAddressResolverOptions().setOptResourceEnabled(false);
        super.beforeStartingVertx(options);
    }

    public static void main(String[] args) {
        // IMPORTANT
        // This is required to use our custom launcher.
        new PiveauLauncher().dispatch(args);
    }

}
