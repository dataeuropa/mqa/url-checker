package io.piveau.metrics.url_checker;

import io.piveau.pipe.connector.PipeConnector;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {
        loadConfig()
                .compose(config -> {
                    DeploymentOptions options = new DeploymentOptions()
//                            .setWorker(true)
//                            .setInstances(config.getInteger(
//                                    ApplicationConfig.ENV_PIVEAU_URLCHECKER_VERTICLE_INSTANCES,
//                                    DeploymentOptions.DEFAULT_INSTANCES))
//                            .setWorkerPoolSize(config.getInteger(
//                                    ApplicationConfig.ENV_PIVEAU_URLCHECKER_WORKER_POOL_SIZE,
//                                    ApplicationConfig.DEFAULT_PIVEAU_URLCHECKER_WORKER_POOL_SIZE
//                            ))
                            .setConfig(config);

                    return vertx.deployVerticle(UrlCheckerVerticle.class.getName(), options);
                })
                .compose(id -> PipeConnector.create(vertx))
                .onSuccess(connector -> {
                    connector.publishTo(UrlCheckerVerticle.ADDRESS);
                    startPromise.complete();
                })
                .onFailure(startPromise::fail);
    }

    private Future<JsonObject> loadConfig() {
        return Future.future(loadConfig -> {
            ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                    .setType("env")
                    .setConfig(new JsonObject().put("keys", new JsonArray()
                            .add(ApplicationConfig.ENV_HTTP_USER_AGENT)
                            .add(ApplicationConfig.ENV_TIMEOUT_IN_MILLIS)
                            .add(ApplicationConfig.ENV_PIVEAU_URLCHECKER_CLIENT_CONNECTION_CONFIG)
                            .add(ApplicationConfig.ENV_PIVEAU_URLCHECKER_VERTICLE_INSTANCES)
                            .add(ApplicationConfig.ENV_PIVEAU_URLCHECKER_WORKER_POOL_SIZE)
                            .add(ApplicationConfig.ENV_PIVEAU_URLCHECKER_CLIENT_POOL_SIZE)));
            ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions)).getConfig(handler -> {
                if (handler.succeeded()) {
                    if (log.isDebugEnabled()) {
                        log.debug(handler.result().encodePrettily());
                    }
                    loadConfig.complete(handler.result());
                } else {
                    loadConfig.fail(handler.cause());
                }
            });
        });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
