# ChangeLog

## Unreleased

## 3.2.3 (2022-04-13)

**Added:**
* Configurable connection settings

**Changed:**
* Set connection keep alive default to false

**Fixed:**
* Missing `PIVEAU_URLCHECKER_CLIENT_POOL_SIZE` retrieving

## 3.2.2 (2021-10-18)

**Changed:**
* Use event loop instead of worker verticle
* Important connector update

## 3.2.1 (2021-06-23)

**Changed:**
* Connector pipe handling

## 3.2.0 (2021-06-07)

**Fixed:**
* Apply correct measurement to dqv 

## 3.1.1 (2021-03-19)

**Fixed:**
* Vert.x issues (updated to 4.0.3)

## 3.1.0 (2021-01-31)

**Changed:**
* Switched to Vert.x 4.0.0

**Added:**
* `dqv:hasQualityMetadata` property for resource when metrics graph is created
* Config `PIVEAU_URLCHECKER_VERTICLE_INSTANCES`, `PIVEAU_URLCHECKER_WORKER_POOL_SIZE`, `PIVEAU_URLCHECKER_CLIENT_POOL_SIZE`

## 3.0.4 (2020-10-26)

**Fixed:**
* Handling of invalid urls

## 3.0.3 (2020-10-25)

**Fixed:**
* Handling access/download url selection

## 3.0.2

**Fixed:**
* Format check to empty to avoid NPE

## 3.0.1 (2020-09-03)

Catch up versioning...

**Fixed:**
* Moved connector creation to main verticle

## 3.0.0 (2020-07-30)

**Added:**
* Licence

**Changed:**
* Processing of geo service URLs

## 2.1.7 (2020-04-09)

Intermediate release

## 2.1.6 (2020-04-09)

**Fixed:**
* Vert.x DNS resolving parameter

## 2.1.5 (2020-04-05)

**Added:**
* Caching of URL addresses without query part

## 2.1.4 (2020-04-03)

**Fixed:**
* Update Vert.x to fix DNS resolution problem 

**Removed:**
* PiveauLauncher (deprecated)

## 2.1.3 (2020-03-26)

**Fixed:**
* Vert.x DNS resolving parameter

## 2.1.2 (2020-03-26)

**Changed:**
* Disable Vert.x own DNS resolving mechanism

## 2.1.1 (2020-03-18)

**Fixed:**
* Catch and handle also illegal argument exception

## 2.1.0 (2020-03-10)

**Added:**
* Support for uriRef in dataInfo

**Fixed:**
* Catch and handle vertx url exception

**Changed:**
* Close dataset explicitly

## 2.0.3 (2020-03-06)

**Fixed:**
* Measurement API

## 2.0.2 (2020-03-05)

**Changed:**
* Web client configuration (timeouts and pool size)

## 2.0.1 (2020-03-05)

**Fixed:**
* Dockerfile start command

## 2.0.0 (2020-02-28)

**Added**
* JSON schema to describe JSON payload in pipe segment description
* PipeContext logging
* Accepts Dataset, Model and Json object
* Api key configuration
* Hub config via pipe

**Changed:**
* Service now only accepts pipe compliant requests
* URL check requests may now only contain either a accessUrl or a downloadUrl
* Results are send back to a preconfigured callback URL as DQV
* Configuration parameters

**Removed**
* Old APIs accepting plain JSON

**Fixed:**
* Extract all access and download URLs from dataset
* Only check rdfs:Resource type URLs
* Measure also invalid URL types

## 1.0.1 (2019-12-13)

**Changed:**
* Tighter integration with the Piveau hub pipe concept
* URL check requests now contain up to two explicitely named URLs; access URL and download URL (optional)
* Results are send back to a preconfigured callback URL as DQV

**Removed:**
* JSON as result content type
* The ability to define callback URLs on request basis


## 1.0.0 (2019-12-05)

**Changed:**
* Bumped Vert.X version to 4.0.0-milestone3
* Changed required Java version to 11
* URLs are checked with a HTTP HEAD instead of GET request

**Removed:**
* Maven Wrapper
* Redeploy scripts

**Fixed:**
* All errors related to the Vert.X version bump
