# Metrics URL Checker

Checks URLs for their status codes.


## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [API](#api)
1. [Configuration](#configuration)
    1. [Environment](#environment)
    1. [Logging](#logging)
1. [License](#license)

## Build

Requirements:
 * Git
 * Maven 3
 * Java 11

```bash
$ git clone ...
$ mvn package
```
 
## Run

```bash
$ java -jar target/metrics-url-checker.jar
```

## Docker

Build docker image:
```bash
$ docker build -t piveau/metrics-url-checker .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 piveau/metrics-url-checker
```

## API

The service must be used as a pipe module and only accepts requests that are pipe compliant. The following types of payload are supported:

* RDF (containing DCAT-AP datasets)
* JSON
    ```json
    {
      "$schema": "http://json-schema.org/draft-07/schema#",
      "$id": "http://io.piveau/resources/url-checker/payload.schema.json",
      "title": "URL Checker",
      "type": "object",
      "required": [ "datasetUri", "distributionUri" ],
      "properties": {
        "datasetUri": {
          "type": "string",
          "description": "The dataset URI."
        },
        "distributionUri": {
          "description": "The distribution URI.",
          "type": "string"
        },
        "oneOf": {
          "accessUrl": {
            "description": "The distribution's access URL.",
            "type": "string"
          },
          "downloadUrl": {
            "description": "The distribution's download URL.",
            "type": "string"
          }
        }
      }
    }
    ```

## Configuration

### Environment

|Key|Description|Default|
|:--- |:---|---:|
|PIVEAU_HTTP_USER_AGENT| The user agent to send with the check requests | Mozilla/5.0 (European Data Portal) Gecko/20100101 Firefox/40.1 |
|PIVEAU_TIMEOUT_IN_MILLIS| HTTP request timeout in milliseconds | 10000 |
|PIVEAU_CONNECTION_EXCEPTION_STATUS| The status code to set in case of exceptions | 1100 |
|PIVEAU_INVALID_URL_STATUS| The status code to set if invalid URLs are provided | 1300 |
|PIVEAU_PIVEAU_URLCHECKER_VERTICLE_INSTANCES| | 1 |
|PIVEAU_PIVEAU_URLCHECKER_WORKER_POOL_SIZE| | 20 |
|PIVEAU_PIVEAU_URLCHECKER_CLIENT_POOL_SIZE| | 100 |

## License

[Apache License, Version 2.0](LICENSE.md)
